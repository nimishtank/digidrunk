jQuery(document).ready( function(e) {

	var mediaupload;
	
	jQuery('#upload_image_button').click(function(e) {
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (mediaupload) {
            mediaupload.open();
            return;
        }
        //Extend the wp.media object
        mediaupload = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });
        //When a file is selected, grab the URL and set it as the text field's value
        mediaupload.on('select', function() {
            attachment = mediaupload.state().get('selection').first().toJSON();
            jQuery('#profile-picture').val(attachment.url);
        });
        //Open the uploader dialog
        mediaupload.open();
    });

});