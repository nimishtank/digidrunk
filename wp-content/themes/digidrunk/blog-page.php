<?php
/**
* Template Name: Blog Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<?php $backgroundImage = get_field('blog_background_image'); ?>
<section id="up" class="pos-rel js-parallax-bg" style="background-image:url(<?php echo $backgroundImage; ?>)">
	<!-- bg-overlay -->
	<div class="bg-overlay-black"></div>
	<!-- pos-rel start -->
	<div class="pos-rel flex-min-height-100vh">
		<div class="padding-top-bottom-120 width-100perc">
			<!-- title start -->
			<h2 class="headline-xxxxl text-center hidden-box after-preloader-anim">
				<span class="anim-slide"><?php the_field('background_headind'); ?></span>
			</h2><!-- title end -->
		</div>
	</div><!-- pos-rel end -->
</section>

<div id="down" class="pos-rel section-bg-light-2" data-midnight="black">
	<div class="pos-rel padding-top-bottom-120">
		<div class="js-isotope-filter-grid-box padding-top-20 container">
			<div class="list list_row list_center list_margin-30px container js-filter-button-box js-scrollanim">
						<button class="list__item js-filter-button anim-fade js-pointer-small js-filter-button-active" data-filter="*">
							<span class="flip-btn text-color-black" data-text="Everything">Everything</span>
						</button>
						<?php
							$args = array(
						               'taxonomy' => 'category',
						               'order'   => 'ASC'
						           );

							$categories = get_categories($args);
							foreach($categories as $category) { ?>
							   
							   	<button class="list__item js-filter-button anim-fade tr-delay-02 js-pointer-small" data-filter=".<?php echo $category->name; ?>">
									<span class="flip-btn text-color-black" data-text="<?php echo $category->name; ?>"><?php echo $category->name; ?></span>
								</button>
						<?php	} ?>	
						<!-- <button class="list__item js-filter-button anim-fade tr-delay-02 js-pointer-small" data-filter=".art">
							<span class="flip-btn text-color-black" data-text="Art">Art</span>
						</button>
						<button class="list__item js-filter-button anim-fade tr-delay-03 js-pointer-small" data-filter=".design">
							<span class="flip-btn text-color-black" data-text="Design">Design</span>
						</button> -->
					</div><!-- filter-buttons end -->
			<?php 
				$args = array(  
			        'post_type' => 'blogs',
			        'post_status' => 'publish',
			        'order' => 'ASC',
			        
			    );

				$loop = new WP_Query( $args );
				//echo "<pre>";print_r($loop);

			
			while ( $loop->have_posts() ) : $loop->the_post(); 
					$category = get_the_category( $post->ID );
					//echo "<pre>"; print_r($category);
					//echo $category[0]->cat_name;
					$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
					?>
			        <article class="padding-top-40 grid-item-50-50-100 js-isotope-filter-grid-item  <?php echo $category[0]->cat_name?>">
						<div class="grid-margin-box hover-box pos-rel js-touch-hover-scroll">
							<a href="<?php echo get_permalink( $post->ID )?>" class="d-block pos-rel hidden-box content-bg-dark-1 js-pointer-large js-animsition-link">
								<img class="img-hover-opacity img-hover-scale in" src="<?php echo $feat_image_url; ?>" alt="Post">
								<!-- bg-overlay -->
								<div class="bg-overlay-black"></div>
								<h3 class="pos-abs pos-left-bottom headline-xxxs hover-move-right"><?php the_title();?></h3>
							</a>
							<ul class="pos-abs pos-left-top list list_row list_margin-30px">
								<li class="list__item">
									<a href="#" class="subhead-xxs hover-text-fill js-pointer-small" data-text="By: Jeffery Reder">By: Jeffery Reder</a>
								</li>
								<li class="list__item">
									<a href="#" class="subhead-xxs hover-text-fill tr-delay-01 js-pointer-small" data-text="In: <?php echo $category[0]->cat_name?>">In: <?php echo $category[0]->cat_name?></a>
								</li>
								<li class="list__item">
									<a href="#" class="subhead-xxs hover-text-fill tr-delay-02 js-pointer-small" data-text="2020, March 15">2020, March 15</a>
								</li>
							</ul>
						</div>
					</article>
			<?php 
			    endwhile;
			?>    
			
		</div>	
	</div>	
</div>
<?php get_footer();?>