<?php
/**
* Template Name: Tech Solutions Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header(); ?>
	<!-- page head start -->
	<?php $backimage = get_field('background_image'); ?>
	<section id="up" class="pos-rel section-bg-dark-1" style="background-image: url(<?php echo $backimage; ?>);background-size: cover;">
		<!-- pos-rel start -->
		<div class="pos-rel flex-min-height-100vh">
			<div class="container padding-top-bottom-120 after-preloader-anim">
				<h3 class="headline-xxxs hidden-box">
					<span class="anim-slide"><?php the_field('page_title'); ?></span>
				</h3>
				<h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('Discription'); ?></h2>
	
			</div>
		</div><!-- pos-rel end -->
	</section><!-- page head end -->

			<!-- work process start -->
	<section id="down" class="pos-rel section-bg-light-1" data-midnight="black">
		<!-- pos-rel start -->
		<div class="pos-rel flex-min-height-100vh">
			<!-- container start -->
			<div class="container ">
				<!-- flex-container start -->
				<div class="flex-container services">
					<!-- column start -->
					<?php if( have_rows('services_tabs') ): ?>
                		<?php while( have_rows('services_tabs') ): the_row(); ?>
							<div class="four-columns column-100-100 ">
								<div class="column-r-margin-40-999 js-scrollanim">
									<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">0<?php the_sub_field('counter_number'); ?></span></span>
									<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
										<span class="anim-slide tr-delay-01"><?php the_sub_field('tab_name'); ?></span>
									</h3>
									<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02"><?php the_sub_field('tab_content'); ?>‌</p>
								</div>
							</div>
						<?php endwhile; ?>
            		<?php endif; ?>			
					<!-- <div class="four-columns column-100-100">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">02</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">VR‌ ‌Marketing‌ </span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">You‌ ‌can‌ ‌use‌ ‌VR‌ ‌to‌ ‌offer‌ ‌a‌ ‌digital‌ ‌experience‌ ‌in‌ ‌place‌ ‌of‌ ‌a‌ ‌physical‌ ‌one,‌ ‌which‌ ‌can‌ promote‌ ‌products‌ ‌and‌ ‌services‌</p>
						</div>
					</div>
					<div class="four-columns column-100-100">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">03</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">3D‌ ‌Content‌ ‌Development‌ </span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">We‌ ‌create‌ ‌and‌ ‌manipulate‌ ‌shapes‌ ‌and‌ ‌models‌ ‌that‌ ‌are‌ ‌fleshed‌ ‌out‌ ‌with‌ ‌textures‌ ‌and‌ ‌other‌ ‌effects‌ ‌through‌ ‌our‌ ‌3D‌ ‌video‌ ‌service‌</p>
						</div>
					</div>
					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">04</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Virtual‌ ‌Events‌</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Producing‌ ‌online‌ ‌events‌ ‌for‌ ‌meetings,‌ ‌quarterly‌ ‌wrap-ups,‌ ‌company‌ ‌trainings,‌ ‌and‌ ‌product‌ ‌rollouts‌ ‌for‌ ‌up‌ ‌to‌ ‌10,000‌ ‌people‌ ‌using‌ ‌the‌ ‌virtual‌ ‌venue‌ ‌that‌ ‌works‌ ‌for‌ ‌you.‌ ‌We‌ ‌take‌ ‌care‌ ‌of‌ ‌everything‌ ‌from‌ ‌start‌ ‌to‌ ‌finish‌ ‌(from‌ ‌platform‌ ‌technology‌ ‌to‌ ‌audience‌communication)‌ ‌</p>
						</div>
					</div>
					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">05</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Chatbots</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Automate‌ ‌your‌ ‌customer‌ ‌service‌ ‌with‌ ChatBot‌‌ ‌and‌ ‌never‌ ‌miss‌ ‌a‌ ‌chance‌ ‌to‌ ‌sell‌ ‌or‌ ‌help‌your‌ ‌customers.‌
						</div>
					</div>
					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box">
								<span class="anim-slide">06</span>
							</span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Marketing Automation</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Gain‌ ‌the‌ ‌power‌ ‌and‌ ‌flexibility‌ ‌you‌ ‌need‌ ‌to‌ ‌engage‌ ‌customers‌ ‌at‌ ‌scale‌ ‌·‌ ‌Attract‌ ‌and‌Convert‌ ‌Prospects‌ ‌·‌ ‌Grow‌ ‌Customer‌ ‌Relationships‌ ‌with‌ ‌our‌ ‌automated‌ ‌marketing‌ services</p>
						</div>
					</div> --><!-- column end -->

				</div><!-- flex-container end -->
			</div><!-- container end -->
		</div><!-- pos-rel end -->
	</section><!-- work process end -->

<?php get_footer(); ?>