<?php
/**
* Template Name: Branding And Solution Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<?php $backimage = get_field('background_image'); ?>
<section id="up" class="pos-rel section-bg-dark-1" style="background-image: url(<?php echo $backimage; ?>);background-size: cover;">
    <div class="pos-rel flex-min-height-100vh">
        <div class="container padding-top-bottom-120 after-preloader-anim">
            <h3 class="headline-xxxs hidden-box"><span class="anim-slide"><?php the_field('page_title'); ?></span></h3>
            <h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('Discription'); ?></h2>
        </div>
    </div>
    <!-- pos-rel end -->

</section><!-- page head end -->

<!-- work process start -->

<section id="down" class="pos-rel section-bg-light-1" data-midnight="black"><!-- pos-rel start -->
    <div class="pos-rel flex-min-height-100vh"><!-- container start -->
        <div class="container ">

            <!-- flex-container start -->
            <div class="flex-container services">
                    <?php if( have_rows('services_tabs') ): ?>
                        <?php while( have_rows('services_tabs') ): the_row(); ?>
                            <div class="four-columns column-100-100 ">
                                <div class="column-r-margin-40-999 js-scrollanim">
                                    <span class="subhead-xxl text-color-red d-block hidden-box"> <span class="anim-slide">0<?php the_sub_field('counter_number'); ?></span> </span>
                                    <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"> <span class="anim-slide tr-delay-01"><?php the_sub_field('tab_name'); ?></span></h3>
                                    <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02"><?php the_sub_field('tab_content'); ?> ‌</p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?> 
                
                <!-- column end -->

                <!-- column start -->
                <!-- <div class="four-columns column-100-100">
                    <div class="column-r-margin-40-999 js-scrollanim">
                        <span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">02</span></span>
                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Visual Language</span></h3>
                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Direct‌ ‌and‌ ‌subliminal‌ ‌communication‌ ‌of‌ ‌a‌ ‌company's‌ ‌values‌ ‌and‌ ‌personality‌ ‌through‌ ‌compelling‌ ‌imagery‌ ‌and‌ ‌design‌ ‌style</p>
                    </div>
                </div>
                
                <div class="four-columns column-100-100">
                    <div class="column-r-margin-40-999 js-scrollanim">
                        <span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">03</span></span>
                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Brand Guidelines </span></h3>
                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02"> ‌When‌ ‌it‌ ‌comes‌ ‌to‌ ‌branding,‌ ‌consistency‌ ‌is‌ ‌everything.With‌ ‌a‌ ‌brand‌ ‌guideline‌ ‌you‌ ‌can‌ ‌outline‌ ‌everything‌ ‌that‌ ‌matters‌ ‌to‌ ‌your‌ ‌brand,‌ ‌from‌ ‌typography‌ ‌to‌ ‌logo‌ ‌to‌ ‌colours‌ ‌to‌ ‌imagery,‌ ‌everything</p>
                    </div>
                </div>
                
                <div class="four-columns column-100-100 ">
                    <div class="column-r-margin-40-999 js-scrollanim">
                        <span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">04</span></span>
                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Collateral‌ ‌Design‌</span></h3>
                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02" style="color: black;">Brand‌ ‌collateral‌ ‌–‌ ‌from‌ ‌packaging‌ ‌and‌ ‌print‌ ‌to‌ ‌business‌ ‌cards‌ ‌and‌ ‌t-shirts‌ ‌–‌ ‌is‌ ‌about‌ getting‌ ‌your‌ ‌brand‌ ‌noticed‌ ‌and‌ ‌strengthening‌ ‌the‌ ‌bond‌ ‌between‌ ‌company‌ ‌and‌ customer.‌ ‌Putting‌ ‌truly‌ ‌memorable‌ ‌collateral‌ ‌in‌ ‌the‌ ‌hands‌ ‌of‌ ‌your‌ ‌customers‌ ‌requires‌ ‌great‌ ‌design‌ ‌–‌ ‌not‌ ‌to‌ ‌mention‌ ‌branding‌ ‌worth‌ ‌showing‌ ‌off‌ ‌in‌ ‌the‌ ‌first‌ ‌place.‌</p>
                    </div>
                </div>
                <div class="four-columns column-100-100 ">
                    <div class="column-r-margin-40-999 js-scrollanim">
                        <span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">05</span></span>
                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Packaging</span></h3>
                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">A coordinated‌ ‌system‌ ‌of‌ ‌preparing‌ ‌goods‌ ‌for‌ ‌transport,‌ ‌warehousing,‌ ‌logistics,‌ ‌sale,‌ ‌and‌ ‌end‌ ‌use.‌ ‌‌Packaging‌‌ ‌contains,‌ ‌protects,‌ ‌preserves,‌ ‌transports,‌ ‌informs,‌ ‌and‌ ‌sells.‌ ‌</p>
                    </div>
                </div>
                <div class="four-columns column-100-100 ">
                    <div class="column-r-margin-40-999 js-scrollanim">
                        <span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">06</span></span>
                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Editorial Design</span></h3>
                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Combining‌ ‌clever‌ ‌compositions,‌ ‌editorial‌ ‌layout,‌ ‌and‌ ‌creative‌ ‌typography.‌ ‌All‌ ‌those‌ ‌elements‌ ‌brought‌ ‌together‌ ‌create‌ ‌an‌ ‌outstanding‌ ‌newspaper‌ ‌layout.‌</p>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section><!-- work process end -->
<?php get_footer(); ?>