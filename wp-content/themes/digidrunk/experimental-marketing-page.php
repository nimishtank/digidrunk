<?php
/**
* Template Name: Experimental Marketing Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header(); ?>
		<!-- page head start -->
	<?php $backimage = get_field('marketing_background_image'); ?>
	<section id="up" class="pos-rel section-bg-dark-1" style="background-image: url(<?php echo $backimage; ?>);background-size: cover;">
		<!-- pos-rel start -->
		<div class="pos-rel flex-min-height-100vh">
			<div class="container padding-top-bottom-120 after-preloader-anim">
				<h3 class="headline-xxxs hidden-box">
					<span class="anim-slide"><?php the_field('marketing_page_title'); ?></span>
				</h3>
				<h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('marketing_description'); ?></h2>
				<?php if( have_rows('marketing_items_name') ): ?>
					<ul class="list subhead-xxs text-color-b0b0b0 margin-top-30 js-scrollanim js-scrollanim-active">
						<?php while( have_rows('marketing_items_name') ): the_row(); ?>
							<li class="list__item red dot hidden-box">
								<p class="anim-slide"><?php the_sub_field('marketing_item_name'); ?></p>
							</li>
						<?php endwhile; ?>	
					</ul>
				<?php endif;?>	
			</div>
		</div><!-- pos-rel end -->
	</section><!-- page head end -->

<?php get_footer(); ?>