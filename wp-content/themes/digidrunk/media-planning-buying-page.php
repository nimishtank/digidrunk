<?php
/**
* Template Name: Media Planning & Buying Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header(); ?>
<!-- page head start -->
			<?php $backimage = get_field('background_image'); ?>
			<section id="up" class="pos-rel section-bg-dark-1" style="background-image: url(<?php echo $backimage; ?>);background-size: cover;">
				<!-- pos-rel start -->
				<div class="pos-rel flex-min-height-100vh">
					<div class="container padding-top-bottom-120 after-preloader-anim">
						<h3 class="headline-xxxs hidden-box">
							<span class="anim-slide"><?php the_field('page_title'); ?></span>
						</h3>
						<h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('Discription'); ?></h2>
					</div>
				</div><!-- pos-rel end -->
			</section><!-- page head end -->

			<!-- work process start -->
			<section id="down" class="pos-rel section-bg-light-1" data-midnight="black">
				<!-- pos-rel start -->
				<div class="pos-rel flex-min-height-100vh">
					<!-- container start -->
					<div class="container ">
						<!-- flex-container start -->
						<div class="flex-container ">
							<!-- column start -->
							<?php if( have_rows('services_tabs') ): ?>
	                    		<?php while( have_rows('services_tabs') ): the_row(); ?>
									<div class="four-columns column-100-100 ">
										<div class="column-r-margin-40-999 js-scrollanim">
											<span class="subhead-xxl text-color-red d-block hidden-box">
												<span class="anim-slide">0<?php the_sub_field('counter_number'); ?></span>
											</span>
											<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
												<span class="anim-slide tr-delay-01"><?php the_sub_field('tab_name'); ?></span>
											</h3>
											<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02"><?php the_sub_field('tab_content'); ?>‌</p>
										</div>
									</div>
								<?php endwhile; ?>
	                		<?php endif; ?>			
							<!-- column end -->

						</div><!-- flex-container end -->
					</div><!-- container end -->
				</div><!-- pos-rel end -->
			</section><!-- work process end -->
<?php get_footer(); ?>