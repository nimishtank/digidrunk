<?php
/**
* Template Name: Social Media Marketing Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<section id="up" class="pos-rel section-bg-dark-1"><!-- pos-rel start -->
    <div class="pos-rel flex-min-height-100vh">
        <div class="container padding-top-bottom-120 after-preloader-anim">
            <h1 class="headline-xxl hidden-box"><span class="anim-slide"><?php the_field('page_title'); ?></span></h1>
            <?php the_field('content'); ?>
        </div>
    </div>
</section>
<section id="down" class="pos-rel section-bg-light-1" data-midnight="black">
    <h3><!-- pos-rel start --></h3>
    <div class="pos-rel flex-min-height-100vh">
        <div class="container ">
            <div class="flex-container ">
                	<?php
						// Check rows exists.
						if( have_rows('social_media_services') ):
							$i = 1;
						    while( have_rows('social_media_services') ) : the_row();
						    	$countter_number = get_sub_field('countter_number');
						    	$service_name = get_sub_field('service_name');
						    	$service_content = get_sub_field('service_content');
						      ?>
                <div class="three-columns column-100-100">
	                 <div class="column-r-margin-40-999 js-scrollanim"><span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">0<?php echo $countter_number; ?></span></span>
	                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01"><?php echo $service_name; ?>‌</span></h3>
	                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02"><?php echo $service_content; ?>‌</p>
	                 </div>
	            </div>
	                 <?php
	                 		$i++;
						    endwhile;
						endif;
						?>
                <!-- column end -->

                <!-- column start -->
	               <!--  <div class="three-columns column-100-100">
	                    <div class="column-r-margin-40-999 js-scrollanim"><span class="subhead-xxl text-color-red d-block hidden-box">
	<span class="anim-slide">02</span>
	</span>
	                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Content Marketing</span></h3>
	                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">In‌ ‌a‌ ‌world‌ ‌of‌ ‌infinite‌ ‌supply,‌ ‌stand‌ ‌out‌ ‌with‌ ‌‌a‌ ‌strategic‌ ‌‌marketing‌‌ ‌approach‌ ‌
	                            focused‌ ‌on‌ ‌creating‌ ‌and‌ ‌distributing‌ ‌valuable,‌ ‌relevant‌ ‌and‌ ‌consistent‌ ‌
	                            content‌‌ ‌and,‌ ‌ultimately,‌ ‌drive‌ ‌profitable‌ ‌customer‌ ‌action.‌ ‌</p>

	                    </div>
	                </div> -->
	                <!-- column end -->

	                <!-- column start -->
	                <!-- <div class="three-columns column-100-100">
	                    <div class="column-r-margin-40-999 js-scrollanim"><span class="subhead-xxl text-color-red d-block hidden-box">
	<span class="anim-slide">03</span>
	</span>
	                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Community‌ ‌Management‌ </span></h3>
	                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Make‌ ‌your‌ ‌brand‌ ‌more‌ ‌humane‌ ‌by‌ ‌caring‌ ‌deeply‌ ‌for‌ ‌your‌ ‌audience.‌ ‌Set‌ ‌a‌ ‌
	                            narrative‌ ‌via‌ ‌word‌ ‌of‌ ‌mouth‌ ‌that‌ ‌makes‌ ‌your‌ ‌brand‌ ‌more‌ ‌aspirational‌ ‌among‌ ‌
	                            potential‌ ‌customers.‌ ‌</p>

	                    </div>
	                </div> -->
	                <!-- column end -->
	                <!-- column start -->
	                <!-- <div class="three-columns column-100-100 ">
	                    <div class="column-r-margin-40-999 js-scrollanim"><span class="subhead-xxl text-color-red d-block hidden-box">
	<span class="anim-slide">04</span>
	</span>
	                        <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Online PR</span></h3>
	                        <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Secure‌ ‌high‌ ‌authority‌ ‌backlinks‌ ‌by‌ ‌attaining‌ ‌mention‌ ‌on‌ ‌influential‌ ‌social‌ ‌
	                            media‌ ‌accounts‌ ‌and‌ ‌blogs.‌ ‌ ‌</p>

	                    </div>
	                </div> -->
                <!-- column end -->

            </div>
            <!-- flex-container end -->

        </div>
        <!-- container end -->

    </div>
    <!-- pos-rel end -->

</section><!-- work process end -->


<?php get_footer(); ?>