<?php
/**
 * digidrunk back compat functionality
 *
 * Prevents digidrunk from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage digidrunk
 * @since digidrunk 1.0.0
 */

/**
 * Prevent switching to digidrunk on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since digidrunk 1.0.0
 */
function digidrunk_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'digidrunk_upgrade_notice' );
}
add_action( 'after_switch_theme', 'digidrunk_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * digidrunk on WordPress versions prior to 4.7.
 *
 * @since digidrunk 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function digidrunk_upgrade_notice() {
	/* translators: %s: WordPress version. */
	$message = sprintf( __( 'digidrunk requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'digidrunk' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since digidrunk 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function digidrunk_customize() {
	wp_die(
		sprintf(
			/* translators: %s: WordPress version. */
			__( 'digidrunk requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'digidrunk' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'digidrunk_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since digidrunk 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function digidrunk_preview() {
	if ( isset( $_GET['preview'] ) ) {
		/* translators: %s: WordPress version. */
		wp_die( sprintf( __( 'digidrunk requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'digidrunk' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'digidrunk_preview' );
