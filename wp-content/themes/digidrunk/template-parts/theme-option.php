<?php
function Theme_admin_page(){
	add_menu_page('Theme Options', 'theme-options', 'manage_options', 'official-theme-settings', 'theme_init', 'dashicons-admin-generic', '100');
	add_submenu_page( 'official-theme-settings', 'Settings', 'Footer', 'manage_options', 'official-theme-settings', 'theme_init');
}

function theme_setting(){
	register_setting( 'footer-option-group-setting', 'contact' );
	register_setting( 'footer-option-group-setting', 'emailtxt' );
	register_setting( 'footer-option-group-setting', 'profile_picture');
	register_setting( 'footer-option-group-setting', 'facebooklinktxt' );
	register_setting( 'footer-option-group-setting', 'twiterlinktxt' );
	register_setting( 'footer-option-group-setting', 'instagramtxt' );
	register_setting( 'footer-option-group-setting', 'linkedintxt' );

	add_settings_section( 'footer-options','Footer Settings', 'theme_footer_option', 'official-theme-settings' );

	add_settings_field( 'contact-text', 'Contact Number', 'blackbox_callback','official-theme-settings','footer-options' );
	add_settings_field( 'emai-text', 'Email', 'email_callback','official-theme-settings','footer-options' );
	add_settings_field( 'facebook-text', 'FaceBook Link', 'facebook_callback','official-theme-settings','footer-options' );
	add_settings_field( 'twitter-text', 'Twitter Link', 'twitter_callback','official-theme-settings','footer-options' );
	add_settings_field( 'instagram-text', 'Instagram Link', 'instagram_callback','official-theme-settings','footer-options' );
	add_settings_field( 'linkedin-text', 'LinkrfIn Link', 'linkedin_callback','official-theme-settings','footer-options' );
	add_settings_field( 'image-text', 'Footer Background', 'profile_callback','official-theme-settings','footer-options' );
}
add_action('admin_menu', 'Theme_admin_page');
add_action('admin_init', 'theme_setting');

function theme_footer_option() {
	//echo 'Customize Footer page setting';
}

function blackbox_callback() {
	$Contact = get_option('contact');
	echo '<input type="text" name="contact" value="'.$Contact.'">';
}

function email_callback() {
	$emailtxt = get_option('emailtxt');
	echo '<input type="text" name="emailtxt" value="'.$emailtxt.'">';
}
function facebook_callback() {
	$facebooklinktxt = get_option('facebooklinktxt');
	echo '<input type="text" name="facebooklinktxt" value="'.$facebooklinktxt.'">';
}
function twitter_callback() {
	$twiterlinktxt = get_option('twiterlinktxt');
	echo '<input type="text" name="twiterlinktxt" value="'.$twiterlinktxt.'">';
}
function instagram_callback() {
	$instagramtxt = get_option('instagramtxt');
	echo '<input type="text" name="instagramtxt" value="'.$instagramtxt.'">';
}
function linkedin_callback() {
	$instagramtxt = get_option('linkedintxt');
	echo '<input type="text" name="linkedintxt" value="'.$linkedintxt.'">';
}

function profile_callback() {
	$uploadfile = get_option('profile_picture');
	echo '<input type="button" value="Upload Background Image" id="upload_image_button"><input type="hidden" id="profile-picture" name="profile_picture" value="'.$uploadfile.'"><br>';
	echo '<img src="'.get_option('profile_picture').'" style="margin-top: 17px; position: relative;right: 27px;border: 1px solid #0006; width: 232px;height: 180px;">';
}

function theme_init() {
	require_once get_template_directory().'/template-parts/templat-options.php';	
}
?>