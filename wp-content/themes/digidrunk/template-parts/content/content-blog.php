<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Digidrunk
 */

?>
<style>.container p{
	font-family: 'Montserrat', sans-serif;
    font-size: 15px;
    font-weight: 200;
    line-height: 2px;}</style>
<section id="up" class="lines-section pos-rel anim-lines js-parallax-bg" style="background-image:url(http://www.digidrunk.in/wp-content/uploads/2020/09/beans-beverage-black-coffee-breakfast-373888-1.jpg)">
				<!-- bg-overlay -->
				<div class="bg-overlay-black"></div>
				<!-- lines-container start -->
				<div class="lines-container pos-rel anim-lines flex-min-height-100vh">
					<div class="padding-top-bottom-120 container small after-preloader-anim">
						<!-- title start -->
						<div class="text-center">
							<h2 class="headline-xl anim-fade"><?php the_title();?></h2>
						</div><!-- title end -->

						<!-- description start -->
						<div class="flex-container description">
							<div class="four-columns column-50-100 padding-top-1 text-center">
								<span class="hidden-box d-inline-block">
									<span class="subhead-xxs anim-reveal">By: <?php the_author();?></span>
								</span>
							</div>
							<div class="four-columns column-50-100 padding-top-1 text-center">
								<span class="hidden-box d-inline-block">
									<span class="subhead-xxs anim-reveal tr-delay-02">In: <?php the_category( '<span class="subhead-xxs anim-reveal tr-delay-02">,' , '</span>' );?></span>
								</span>
							</div>
							<div class="four-columns column-50-100 padding-top-1 text-center">
								<span class="hidden-box d-inline-block">
									<span class="subhead-xxs anim-reveal tr-delay-04"><?php the_date();?></span>
								</span>
							</div>
						</div><!-- description end -->
					</div>
				</div><!-- lines-container end -->
			</section><!-- lines-section end -->

			<!-- post start -->
			<div id="down" class="lines-section pos-rel black-lines section-bg-dark-1" data-midnight="black">
				<!-- lines-container start -->
				<div class="lines-container pos-rel no-lines padding-top-bottom-120">
					<!-- entry-content start -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="container small">
							<?php the_content();?>
							
						
						</div>
					</article><!-- entry-content end -->

					<div class="padding-top-60 text-center">
						<span class="subhead-xxs text-color-888888">Share this article:</span>
						<?php echo do_shortcode('[Sassy_Social_Share]'); ?>
						
						<!-- <ul class="list list_row list_center padding-top-10 js-scrollanim">
							<li class="list__item">
								<div class="anim-fade">
									<a href="https://www.instagram.com/share?url=<?php the_permalink(); ?>" class="icon-overlay-btn black js-pointer-large" target="_blank">
										<i class="icon-overlay-btn__inner fab fa-instagram"></i>
									</a>
								</div>
							</li>
							<li class="list__item">
								<div class="anim-fade tr-delay-01">
									<a href="http://twitter.com/share?url=<?php the_permalink(); ?>" class="icon-overlay-btn black js-pointer-large" target="_blank">
										<i class="icon-overlay-btn__inner fab fa-twitter"></i>
									</a>
								</div>
							</li>
							<li class="list__item">
								<div class="anim-fade tr-delay-02">
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>" class="icon-overlay-btn black js-pointer-large" target="_blank">
										<i class="icon-overlay-btn__inner fab fa-facebook-f"></i>
									</a>
								</div>
							</li>
							 <li class="list__item">
								<div class="anim-fade tr-delay-03">
									<a href="#" class="icon-overlay-btn black js-pointer-large" target="_blank">
										<i class="icon-overlay-btn__inner fab fa-youtube"></i>
									</a>
								</div>
							</li>
							<li class="list__item">
								<div class="anim-fade tr-delay-04">
									<a href="#" class="icon-overlay-btn black js-pointer-large" target="_blank"> 
										<i class="icon-overlay-btn__inner fab fa-behance"></i>
									</a>
								</div>
							</li>
						</ul> -->
					</div><!-- post share end -->

				</div><!-- lines-container end -->
			</div><!-- post end -->


		

	

		