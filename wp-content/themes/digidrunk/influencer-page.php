<?php
/**
* Template Name: Influencer Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<?php 
	$backgroungimage = get_field('influencer_background_image'); 
	$pageTitle = get_field('influencer_page_title');
?>
<section id="up" class="pos-rel section-bg-dark-1" style="background-image:url(<?php echo $backgroungimage; ?>)">
				<!-- pos-rel start -->
	<div class="pos-rel flex-min-height-100vh">
		<div class="container padding-top-bottom-120 after-preloader-anim">
			<h3 class="headline-xxxs hidden-box">
				<span class="anim-slide"><?php echo $pageTitle; ?></span>
			</h3>
			<h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('influencer_content'); ?>‌</h2>

		</div>
	</div><!-- pos-rel end -->
</section>

<?php get_footer(); ?>