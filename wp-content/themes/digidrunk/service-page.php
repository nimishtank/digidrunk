<?php
/**
* Template Name: Service Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<!-- page head start -->
	<section id="up" class="pos-rel section-bg-dark-1">
		<!-- pos-rel start -->
		<div class="pos-rel flex-min-height-100vh">
			<div class="container padding-top-bottom-120 after-preloader-anim">
				<h3 class="headline-xxxs hidden-box" style="font-size:50px">
					<span class="anim-slide"><?php the_field('page_title'); ?></span>
				</h3>
				<h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('service_content'); ?>
				</h2>
			</div>
		</div><!-- pos-rel end -->
	</section><!-- page head end -->

	<section id="down" class="pos-rel section-bg-dark-1">
		<!-- pos-rel start -->
		<div class="pos-rel flex-min-height-100vh">
			<div class="pos-rel flex-min-height-100vh">
				<!-- width-100perc start -->
				<div class="width-100perc padding-bottom-120">
					<!-- padding-top-90 start -->
					<div class="padding-top-120">


						<!-- accordion start -->
						<div class="accordion accordion_underline js-accordion margin-top-60">
						<?php if( have_rows('service_faq') ):
							$i = 1;
						    while( have_rows('service_faq') ) : the_row();
						        $service_title = get_sub_field('servicer_title'); 
						        $serviceContent = get_sub_field('service_faqu_content'); ?>

						       <?php  if($i == 4 ) { ?>

								<div class="accordion__tab js-accordion-tab">
									<div class="accordion__btn js-accordion-btn container js-pointer-large">
										<h6 class="accordion__btn-title headline-xs margin-top-bottom-30"><?php echo $service_title; ?>  </h6>
									</div>
									<div class="accordion__content js-accordion-content container">

										<p class="body-text-l margin-top-20 margin-bottom-20"><?php echo $serviceContent; ?>
											 </p><a class="border-btn js-pointer-large margin-top-20" href="http://www.digidrunk.in/influencer-marketing/" target="_blank" rel="noopener noreferrer"> <span class="border-btn__inner">see details</span> </a>


									</div>

								</div>
						<?php	 }	else { ?>

							<div class="accordion__tab js-accordion-tab">
								<div class="accordion__btn js-accordion-btn container js-pointer-large">
									<h6 class="accordion__btn-title headline-xs margin-top-bottom-30"><?php echo $service_title; ?> </h6>
								</div>
								<div class="accordion__content js-accordion-content container">
									<ul class="list subhead-xxs text-color-dadada margin-top-30">
										 <?php    
											 if( have_rows('list_sub_heading') ):
			            						while( have_rows('list_sub_heading') ) : the_row(); 
			            						$List__item = get_sub_field('sub_heading_title');
			            						$Body_text = get_sub_field('sublist_content');	
			            						?>

			            						<li class="list__item dot red "><?php echo $List__item; ?> </li>
										<p class="body-text-l margin-top-20 margin-bottom-20"><?php echo $Body_text; ?></p>
										<?php
			            						endwhile;
			        						endif;?>				
									</ul>
									<a class="border-btn js-pointer-large margin-top-20" href="http://www.digidrunk.in/social-media-marketing/" target="_blank" rel="noopener noreferrer"> <span class="border-btn__inner">see details</span> </a>
								</div>
							</div><!-- accordion__tab end -->		
						<?php } ?>
						<?php	 	
						 	$i++;
						 endwhile;
						endif; ?>	
						</div>
					</div>
					<!-- accordion end -->

				</div>
			</div><!-- width-100perc end -->
		</div><!-- pos-rel end -->
	</section><!-- services end -->

<?php get_footer(); ?>