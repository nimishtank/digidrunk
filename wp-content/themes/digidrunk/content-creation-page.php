<?php
/**
* Template Name: Content Creation Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<!-- page head start -->
<?php $backimage = get_field('background_image'); ?>
	<section id="up" class="pos-rel" style="background-image: url(<?php echo $backimage; ?>);background-size: cover;">
		<!-- pos-rel start -->
		<div class="pos-rel flex-min-height-100vh">
			<div class="container padding-top-bottom-120 after-preloader-anim">
				<h3 class="headline-xxxs hidden-box"><span class="anim-slide">Content Creation</span></h3>
				<h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03">Content‌ ‌is‌ ‌king.‌ ‌And‌ ‌we‌ ‌must‌ ‌bow‌ ‌down.‌ ‌Content‌ ‌shapes‌ ‌up‌ ‌the‌ ‌impression‌ ‌of‌ ‌your‌ ‌brand‌for‌ ‌existing‌ ‌as‌ ‌well‌ ‌as‌ ‌potential‌ ‌customers.‌If‌ ‌you‌ ‌serve‌ ‌your‌ ‌consumers‌ ‌any‌ ‌unappetising‌ ‌content,‌ ‌they‌ ‌will‌ ‌leave‌ ‌you‌ ‌for‌ ‌another‌ ‌brand.‌At‌ ‌DigiDrunk,‌ ‌your‌ ‌brand‌ ‌gets‌ ‌to‌ ‌experience‌ ‌a‌ ‌‌seamless‌ ‌journey‌‌ ‌across‌ ‌all‌ ‌‌media‌ ‌channels‌,maximising‌ ‌the‌ ‌capabilities‌ ‌of‌ ‌desired‌ ‌platforms‌ ‌in‌ ‌‌your‌ ‌favour.</h2>
	
			</div>
		</div><!-- pos-rel end -->
	</section><!-- page head end -->

	<!-- work process start -->
	<section id="down" class="pos-rel section-bg-light-1" data-midnight="black">
		<!-- pos-rel start -->
		<div class="pos-rel flex-min-height-100vh">
			<!-- container start -->
			<div class="container ">
				

				<!-- flex-container start -->
				<div class="flex-container services">
					<!-- column start -->
					<?php if( have_rows('services_tabs') ): ?>
                        <?php while( have_rows('services_tabs') ): the_row(); ?>
                            <div class="four-columns column-100-100 ">
                                <div class="column-r-margin-40-999 js-scrollanim">
                                    <span class="subhead-xxl text-color-red d-block hidden-box"> <span class="anim-slide">0<?php the_sub_field('counter_number'); ?></span> </span>
                                    <h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"> <span class="anim-slide tr-delay-01"><?php the_sub_field('tab_name'); ?></span></h3>
                                    <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02"><?php the_sub_field('tab_content'); ?> ‌</p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?> 
                    
					<!-- <div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box">
								<span class="anim-slide">01</span>
							</span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Video‌ ‌Production</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">We‌ ‌can‌ ‌handle‌ ‌your‌ ‌video‌ ‌production‌ ‌from‌ ‌start‌ ‌to‌ ‌finish.‌ ‌It‌ ‌can‌ ‌be‌ ‌a‌ ‌corporate‌ ‌video‌,TV‌ ‌commercial‌ ‌,‌ ‌radio‌ ‌jingle‌ ‌or‌ ‌any‌ ‌social‌ ‌medium‌ ‌like‌ ‌a‌ ‌YouTube‌ ‌brand‌ ‌film</p>
						</div>
					</div>

					<div class="four-columns column-100-100">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">02</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Photography‌ ‌Styling‌</span></h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">We‌ ‌understand‌ ‌the‌ ‌power‌ ‌of‌ ‌a‌ ‌visually‌ ‌captivating‌ ‌photo‌ ‌and‌ ‌what‌ ‌it‌ ‌can‌ ‌do‌ ‌for‌ ‌your‌product,‌ ‌your‌ ‌goods,‌ ‌your‌ ‌brand.‌ ‌We‌ ‌combine‌ ‌our‌ ‌dynamic‌ ‌skills‌ ‌behind‌ ‌and‌ ‌in‌ ‌front‌ ‌of‌ ‌the‌ ‌lens‌ ‌to‌ ‌photograph‌ ‌and‌ ‌style‌ ‌your‌ ‌product.‌ ‌</p>
						</div>
					</div>

					<div class="four-columns column-100-100">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">03</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Script‌ ‌Development‌</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">The‌ ‌creative‌ ‌process‌ ‌from‌ ‌the‌ ‌inception‌ ‌of‌ ‌an‌ ‌idea‌ ‌through‌ ‌the‌ ‌countless‌ ‌orbits‌ ‌of‌ ‌writing‌ ‌and‌ ‌rewriting</p>
						</div>
					</div>

					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">04</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Copywriting</span></h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Copies‌ ‌that‌ ‌sells‌ ‌your‌ ‌product‌ ‌or‌ ‌service‌ ‌and‌ ‌convinces‌ ‌prospective‌ ‌customers‌ ‌to‌take‌ ‌action.‌ ‌In‌ ‌many‌ ‌ways,‌ ‌it's‌ ‌like‌ ‌hiring‌ ‌one‌ ‌salesman‌ ‌to‌ ‌reach‌ ‌all‌ ‌of‌ ‌your Customer.</p>
						</div>
					</div>
					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box">
								<span class="anim-slide">05</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Storyboarding</span></h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">It’s‌ ‌an‌ ‌important‌ ‌part‌ ‌of‌ ‌the‌ ‌preproduction‌ ‌process‌ ‌and‌ ‌consists‌ ‌of‌ ‌a‌ ‌series‌ ‌of‌ ‌images‌ that‌ ‌show‌ ‌everything‌ ‌that’s‌ ‌going‌ ‌to‌ ‌happen‌ ‌in‌ ‌your‌ ‌finished‌ ‌piece.‌ </p>
						</div>
					</div>
					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">06</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box"><span class="anim-slide tr-delay-01">Illustration‌</span></h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">The‌ ‌best‌ ‌way‌ ‌to‌ ‌explain‌ ‌an‌ ‌idea‌ ‌or‌ ‌tell‌ ‌a‌ ‌story‌ ‌or‌ ‌provide‌ ‌decoration.</p>
						</div>
					</div>

					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">07</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Animation</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Our‌ ‌team‌ ‌is‌ ‌proficient‌ ‌to‌ ‌animate‌ ‌everything‌ ‌imaginable.‌ ‌They‌ ‌might‌ ‌help‌ ‌explain‌ ‌an‌idea‌ ‌or‌ ‌tell‌ ‌a‌ ‌story‌ ‌or‌ ‌provide‌ ‌decoration.‌</p>
						</div>
					</div>

					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">08</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Motion Graphics</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">It’s‌ ‌a‌ ‌highly‌ ‌versatile‌ ‌storytelling‌ ‌tools,‌ ‌which‌ ‌makes‌ ‌them‌ ‌a‌ ‌great‌ ‌addition‌ ‌to‌ ‌your‌brand's‌ ‌content‌ ‌marketing‌ ‌mix‌ ‌</p></div>
					</div>

					<div class="four-columns column-100-100 ">
						<div class="column-r-margin-40-999 js-scrollanim">
							<span class="subhead-xxl text-color-red d-block hidden-box"><span class="anim-slide">09</span></span>
							<h3 class="headline-xxxs text-color-black margin-top-30 hidden-box">
								<span class="anim-slide tr-delay-01">Explainer‌ ‌Videos‌</span>
							</h3>
							<p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02">Illustrate‌ ‌complex‌ ‌ideas‌ ‌in‌ ‌simple,‌ ‌engaging,‌ ‌and‌ ‌meaningful‌ ‌ways‌ ‌in‌ ‌less‌ ‌than‌ ‌2‌minutes.‌ ‌Being‌ ‌one‌ ‌of‌ ‌the‌ ‌most‌ ‌potent‌ ‌resources‌ ‌for‌ ‌content‌ ‌marketers‌ ‌today,‌ ‌they‌ ‌are‌ ‌aimed‌ ‌at‌ ‌describing‌ ‌your‌ ‌company’s‌ ‌products‌ ‌(or‌ ‌services)‌ ‌in‌ ‌a‌ ‌way‌ ‌that‌resonates‌ ‌with‌ ‌your‌ ‌target‌ ‌audience’s‌ ‌pain-points‌</p>
						</div>
					</div> --><!-- column end -->
				</div><!-- flex-container end -->
			</div><!-- container end -->
		</div><!-- pos-rel end -->
	</section><!-- work process end -->
<?php get_footer(); ?>