<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage digidrunk
 * @since digidrunk 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Jinna Gik">
		<meta name="description" content="UMAYA Template is a uniquely HTML5 template develop in HTML with a modern look.">
		<meta name="keywords" content="creative, modern, clean, html5, css3, portfolio, blog, agency, templates, minimal">

		<!-- favicon -->
		<!-- <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png"> -->
		<!-- <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-touch-icon-57x57.png"> -->
		<!-- <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-touch-icon-72x72.png"> -->
		<!-- <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-touch-icon-114x114.png"> -->

		<!-- fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

		<!-- styles -->
		<link href="<?php echo get_template_directory_uri(); ?>/assets/css/plugins.css" rel="stylesheet" type="text/css">
		<link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet" type="text/css">
	<?php wp_head(); ?>
</head>

<body class="preloader cursor-anim-enable dark-nav" <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
	$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
?>
<div id="page" class="site">
	
		<!-- preloader-loading start -->
		<div class="preloader__logoload-box">
			<img class="preloader__logo" src="<?php echo $image[0]; ?>" alt="logo">
			<div class="preloader__pulse"></div>
		</div><!-- preloader-loading end -->

		<!-- pointer start -->
		<div class="pointer js-pointer" id="js-pointer">
			<i class="pointer__inner fas fa-long-arrow-alt-right"></i>
			<i class="pointer__inner fas fa-search"></i>
			<i class="pointer__inner fas fa-link"></i>
		</div><!-- pointer end -->

		<!-- to top btn start -->
		<a href="#up" class="scroll-to-btn js-headroom js-midnight-color js-smooth-scroll js-pointer-large">
			<span class="scroll-to-btn__box">
				<span class="scroll-to-btn__arrow"></span>
			</span>
		</a><!-- to top btn end -->

		<!-- scroll down btn start -->
		<a href="#down" class="scroll-to-btn to-down js-headroom js-midnight-color js-smooth-scroll js-pointer-large js-scroll-btn">
			<span class="scroll-to-btn__box">
				<span class="scroll-to-btn__arrow"></span>
			</span>
		</a><!-- scroll down btn end -->
		<!-- header start -->
		<header class="fixed-header">
			<!-- logo start -->
			<div class="header-logo js-midnight-color js-headroom">
				<div class="hidden-box">
					<a href="index.html" class="header-logo__box js-pointer-large js-animsition-link">
						<img class="header-logo__img white" src="<?php echo $image[0]; ?>" alt="logo">
						<!--<img class="header-logo__img black" src="<?php //echo get_template_directory_uri(); ?>/assets/images/logo/logo-black.png" alt="logo">!-->
					</a>
				</div>
			</div><!-- logo end -->
			
			<!-- menu-icon start -->
			<div class="menu-icon vertical js-menu-open-close js-pointer-large js-midnight-color js-headroom">
				<div class="menu-icon__box">
					<span class="menu-icon__inner"></span>
					<span class="menu-icon__close"></span>
				</div>
			</div><!-- menu-icon end -->

			<!-- header-contact start -->
			<div class="header-contact js-midnight-color">
				<div class="header-contact__flex">
					<div class="header-contact__anim">
						<a href="#" class="header-contact__btn vertical-text center js-pointer-large">
							<span class="vertical-text__inner"><i class="far fa-comment-dots"></i> Get in touch</span>
						</a>
					</div>
				</div>
			</div><!-- header-contact end -->

			<!-- header-social start -->
			<div class="header-social after-preloader-anim js-midnight-color">
				<ul class="list list_center list_margin-20px hidden-box">
					<li class="list__item">
						<div class="hidden-box d-inline-block">
							<a href="<?php echo get_option('facebooklinktxt'); ?>" class="anim-slide js-pointer-small">
								<i class="fab fa-facebook-f"></i>
							</a>
						</div>
					</li>
					<li class="list__item">
						<div class="hidden-box d-inline-block">
							<a href="<?php echo get_option('twiterlinktxt'); ?>" class="anim-slide tr-delay-02 js-pointer-small">
								<i class="fab fa-twitter"></i>
							</a>
						</div>
					</li>
					<li class="list__item">
						<div class="hidden-box d-inline-block">
							<a href="<?php echo get_option('instagramtxt'); ?>" class="anim-slide tr-delay-04 js-pointer-small">
								<i class="fab fa-instagram"></i>
							</a>
						</div>
					</li>
					<li class="list__item">
						<div class="hidden-box d-inline-block">
							<a href="#" class="anim-slide tr-delay-10 js-pointer-small">
								<i class="fab fa-youtube"></i>
							</a>
						</div>
					</li>
					<li class="list__item">
						<div class="hidden-box d-inline-block">
							<a href="<?php echo get_option('linkedintxt'); ?>" class="anim-slide tr-delay-06 js-pointer-small">
								<i class="fab fa-linkedin"></i>
							</a>
						</div>
					</li>
					
				</ul>
			</div><!-- header-social end -->
		</header><!-- header end -->

		<!-- navigation start -->
		<nav class="nav-container lines-section pos-rel black-lines js-dropdown-active-box">
			<!-- lines-container start -->
			<div class="lines-container pos-rel black-lines height-100perc">
				<!-- dropdown close btn start -->
				<div class="dropdown-close">
					<div class="dropdown-close__inner js-dropdown-close js-pointer-large">
						<span class="dropdown-close__arrow"></span>
					</div>
				</div><!-- dropdown close btn end -->

				<!-- js-nav-slider-bg start -->
					
				<!-- menu-box start -->
				
				<?php
					$defaults = array(
						'theme_location'  => 'Primary',
						'menu'            => 'header-menu',
						'container'       => false,
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="menu-box">%3$s</ul>',
						'depth'           => 2,
						'walker'          => ''
						);
					wp_nav_menu( $defaults );

					$args = array(
						'sort_column'  => 'menu_order, post_title',
						'post_status'  => 'publish' 
					); 
						
					wp_list_pages( $args );
					wp_list_pages('&title_li=&link_before="<span class="123">"&link_after="</span>"&depth=2'); 
				?>
				
				
				
				<!--<ul class="menu-box">
					<!-- nav-btn-box start -->
				<!-- nav-btn-box start -->
					<!--<li class="nav-btn-box">
						<a href="http://www.digidrunk.in" class="nav-btn large dropdown-hidden-btn js-animsition-link js-pointer-large">
							<span class="nav-btn__inner" data-text="Home">Home</span>
						</a>
					</li><!-- nav-btn-box end -->
					<!-- nav-btn-box start -->
				

					<!-- nav-btn-box start -->
					<!--<li class="nav-btn-box">
						<a href="http://www.digidrunk.in/services" class="nav-btn large dropdown-hidden-btn js-animsition-link js-pointer-large">
							<span class="nav-btn__inner" data-text="Services">Services</span>
						</a>
					</li><!-- nav-btn-box end -->
					<!-- nav-btn-box start -->
					<!--<li class="nav-btn-box">
						<a href="http://www.digidrunk.in/blog" class="nav-btn large dropdown-hidden-btn js-animsition-link js-pointer-large">
							<span class="nav-btn__inner" data-text="Blog">Blog</span>
						</a>
					</li><!-- nav-btn-box end -->
			
					
					<!-- nav-btn-box start -->
					<!-- <li class="nav-btn-box">
						<a href="http://www.digidrunk.in/contact/" class="nav-btn large dropdown-hidden-btn js-animsition-link js-pointer-large">
							<span class="nav-btn__inner" data-text="Contact">Contact</span>
						</a>
					</li><!-- nav-btn-box end -->
				<!--</ul> menu-box end -->

				<!-- nav-information start -->
				<div class="nav-information">
					<!-- nav-email start -->
					<div>
						<div class="hidden-box d-inline-block">
							<div class="headline-xxxxs nav-title-color nav-reveal-anim js-nav-anim">Email</div>
						</div>
						<div class="nav-fade-anim js-nav-anim margin-top-10">
							<i class="far fa-envelope icon"></i><a href="mailto:hello@digidrunk.in‌" class="subhead-xxs nav-text-color text-hover-to-red js-pointer-small"><?php echo get_option('emailtxt'); ?>‌</a><br>
							
						</div>
					</div><!-- nav-email end -->

					<!-- nav-address start -->
					<!-- nav-address end -->

					<!-- nav-phone start -->
					<div>
						<div class="hidden-box d-inline-block">
							<div class="headline-xxxxs nav-title-color nav-reveal-anim js-nav-anim">Phone</div>
						</div>
						<div class="nav-fade-anim js-nav-anim margin-top-10">
							<i class="fab fa-whatsapp icon"></i><a target="_blank" href="https://api.whatsapp.com/send?phone='..<?php echo get_option('contact'); ?>'" class="subhead-xxs text-color-b0b0b0 text-hover-to-white js-pointer-small"><?php echo get_option('contact'); ?></a><br>
							
						</div>
					</div><!-- nav-phone end -->
				</div><!-- nav-information end -->	

				<!-- nav-information start -->
			</div><!-- lines-container end -->
		</nav><!-- navigation end -->

		<!-- main start -->
		<main class="js-animsition-overlay" data-animsition-overlay="true">