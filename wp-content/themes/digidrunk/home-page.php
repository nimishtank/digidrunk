<?php
/**
* Template Name: Home Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
	<?php $image = get_field('backgroung_image');?>
	<section id="up" class="lines-section pos-rel anim-lines bg-img-cover" style="background-image:url(<?php echo $image; ?>)">
		<!-- bg-overlay -->
		<div class="bg-overlay-black"></div>
		<!-- lines-container start -->
		<div class="lines-container pos-rel anim-lines flex-min-height-100vh border-box-bottom">
			<div class="padding-top-bottom-120 container">
				<!-- title start -->
				<h2 class="headline-xl after-preloader-anim">
					<?php the_field('backgroung_image_content'); ?>
				</h2><!-- title end -->
			</div>
		</div><!-- lines-container end -->
		<!-- particles -->
		<div id="js-particles"></div>
	</section><!-- page head end -->
	
	<!-- about us start -->
	<?php $aboutimage = get_field('background_image_about_setion');?>
	<section id="down" class="lines-section pos-rel section-bg-dark-1" style="background-image: url(<?php echo $aboutimage; ?>)">
		<div class="lines-container pos-rel flex-min-height-100vh border-box-bottom">
			<div class="padding-top-bottom-120 width-100perc">
				<h2 class="headline-xxl text-center js-scrollanim"><span class="hidden-box d-block"> ‌<span class="anim-slide"> I‌nnovation‌ ‌On‌ ‌The‌ ‌House‌ </span> </span> <span class="hidden-box d-block"> <span class="anim-slide tr-delay-01 text-color-red"> ‌Bring‌ ‌Your‌ ‌Own‌ ‌Brief‌ ‌ ‌</span> </span></h2>
				<div class="flex-container padding-top-30">
					<div class="four-columns column-100-100 padding-top-60">
						<div class="line-col-l-r-margin-20 js-scrollanim">
							<h6 class="hidden-box"><span class="subhead-xs text-color-red anim-slide"><?php the_field('about_title'); ?></span></h6>
							<p class="body-text-s margin-top-20 anim-text-reveal tr-delay-02"><?php the_field('about_content'); ?></p>

						</div>
						<div class="margin-top-20 margin-bottom-20" style="border-bottom: 1px solid rgba(250, 250, 250, .1); margin-top: 10px;"></div>
						<div class="line-col-l-r-margin-20 js-scrollanim"></div>
					</div>
					<div class="four-columns column-50-100 padding-top-60">
							
						<?php 
							if( have_rows('what-we-section') ): 
							$i = 1;
							 while( have_rows('what-we-section') ): the_row(); ?>
								
								<?php if($i == 2 ) { ?>
									<div class="margin-top-20 margin-bottom-20" style="border-bottom: 1px solid rgba(250, 250, 250, .1); margin-top: 10px;"></div>
										<div class="line-col-l-r-margin-20 js-scrollanim">
											<h6 class="hidden-box"><span class="subhead-xs text-color-red anim-slide"><?php the_sub_field('heading'); ?></span></h6>
											<p class="body-text-s margin-top-20 anim-text-reveal tr-delay-02">‌<?php the_sub_field('content'); ?></p>

										</div>
									<div class="margin-top-20 margin-bottom-20" style="border-bottom: 1px solid rgba(250, 250, 250, .1); margin-top: 10px;"></div>
								<?php } else { ?>
								<div class="line-col-l-r-margin-20 js-scrollanim">
									<h6 class="hidden-box"><span class="subhead-xs text-color-red anim-slide"><?php the_sub_field('heading'); ?></span></h6>
									<p class="body-text-s margin-top-20 anim-text-reveal tr-delay-02">‌‌‌<?php the_sub_field('content'); ?></p>
								</div>
								<?php } ?>
								
						<?php 
							$i++;
							endwhile;
						 	endif; 
						 ?>	
					</div>
					<div class="four-columns column-50-100 padding-top-60">
						<div class="line-col-l-r-margin-20 js-scrollanim">
							<h6 class="hidden-box"><span class="subhead-xs text-color-red anim-slide"><?php the_field(' digicrew-heading- title'); ?></span></h6>
							<ul class="list list_counter text-color-b0b0b0 margin-top-20 anim-text-reveal tr-delay-02">
								
								<?php if( have_rows('digicrew_section') ): ?>
									<?php while( have_rows('digicrew_section') ): the_row(); ?>
										<li class="list__item red">
											<p class="subhead-xxs"><?php the_sub_field('subheading'); ?>‌</p>
										</li>
									<?php endwhile; ?>
									
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section><!-- about us end -->
	
	<!--<section class="pos-rel section-bg-dark-1" data-midnight="black" style="background-image: url(http://www.digidrunk.in/wp-content/uploads/2020/10/download-1.jpg);">!-->
	<section class="pos-rel section-bg-dark-1" style="background-image: url(http://www.digidrunk.in/wp-content/uploads/2020/10/download-1.jpg);">
		<div class="pos-rel flex-min-height-100vh">
			<div class="container flex-container padding-top-60 padding-bottom-120">
				<div class="five-columns column-100-100 padding-top-60">
					<div class="column-r-margin-40-999 js-scrollanim js-scrollanim-active">
						<h2 class="headline-m text-color-red hidden-box"><span class="anim-slide"><?php the_field('service_heading'); ?></span></h2>
					</div>
				</div>
				<div class="seven-columns column-100-100 padding-top-60">
					<div class="accordion accordion_black-underline js-accordion padding-top-60">
						<?php if( have_rows('faq') ): ?>
									<?php 
										$k=1;
									while( have_rows('faq') ): the_row(); ?>


									<?php if($k == 4 ) { ?>
											<div class="accordion__tab js-accordion-tab">
												<div class="accordion__btn js-accordion-btn js-pointer-large">
													<h6 class="accordion__btn-title black-plus-icon headline-xxxxs text-color-b0b0b0 margin-top-bottom-20">‌ ‌Influencer‌ ‌Marketing‌</h6>
												</div>
												<div class="accordion__content js-accordion-content">
													<p class="body-text-xs text-color-b0b0b0">High‌ ‌amount‌ ‌of‌ ‌trust‌ ‌that‌ ‌social‌ ‌influencers‌ ‌have‌ ‌built‌ ‌up‌ ‌with‌ ‌their‌ ‌following,‌ ‌ and‌ ‌recommendations‌ ‌from‌ ‌them‌ ‌serve‌ ‌as‌ ‌a‌ ‌form‌ ‌of‌ ‌‌<b> social‌ ‌proof‌‌ </b> ‌to‌ ‌your‌ ‌brand’s‌ ‌ potential‌ ‌customers.‌ ‌</p>
													<a class="border-btn js-pointer-large margin-top-20" href="http://www.digidrunk.in/influencer-marketing/" target="_blank" rel="noopener"> <span class="border-btn__inner">see details</span> </a>

												</div>
											</div>
										<?php } else if($k == 1) { ?>
										
										<div class="accordion__tab js-accordion-tab">
											<div class="accordion__btn js-accordion-btn js-pointer-large js-accordion-actives">
												<h6 class="accordion__btn-title black-plus-icon headline-xxxxs text-color-b0b0b0 margin-top-bottom-20"><?php the_sub_field('faq_title'); ?>‌</h6>
											</div>
											<div class="accordion__content js-accordion-content" style="display: block;">
												<div class="line-col-l-r-margin-20 js-scrollanim js-scrollanim-active">
														<ul class="list list_counter text-color-b0b0b0 margin-top-20 anim-text-reveal tr-delay-02">
														<?php while( have_rows('faq_last') ): the_row(); ?>
																<li class="list__item red">
																	<p class="subhead-xxss">‌<?php $serviceName = the_sub_field('sub_heading_title'); ?></p>
																</li>
															
														<?php endwhile; ?>	
														</ul>
													<a class="border-btn js-pointer-large margin-top-20" href="<?php the_sub_field('faq_link'); ?>" target="_blank" rel="noopener"> <span class="border-btn__inner">see details</span> </a>
												</div>
											</div>
										</div>
										<?php } else { ?>
										<div class="accordion__tab js-accordion-tab">
											<div class="accordion__btn js-accordion-btn js-pointer-large">
												<h6 class="accordion__btn-title black-plus-icon headline-xxxxs text-color-b0b0b0 margin-top-bottom-20"><?php the_sub_field('faq_title'); ?>‌</h6>
											</div>
											<div class="accordion__content js-accordion-content">
												<div class="line-col-l-r-margin-20 js-scrollanim js-scrollanim-active">
														<ul class="list list_counter text-color-b0b0b0 margin-top-20 anim-text-reveal tr-delay-02">
														<?php while( have_rows('faq_last') ): the_row(); ?>
																<li class="list__item red">
																	<p class="subhead-xxss">‌<?php $serviceName = the_sub_field('sub_heading_title'); ?></p>
																</li>
															
														<?php endwhile; ?>	
														</ul>
													<a class="border-btn js-pointer-large margin-top-20" href="<?php the_sub_field('faq_link'); ?>" target="_blank" rel="noopener"> <span class="border-btn__inner">see details</span> </a>
												</div>
											</div>
										</div>
										
										<?php } ?>
									<?php 
										$k++;	
									endwhile; ?>
									
								<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>	
	
	<!-- portfolio start -->
	<?php $recentbackgroundimage = get_field('background_image_recent_work');?>
	<section class="lines-section pos-rel section-bg-dark-1" style="background-image: url(<?php echo $recentbackgroundimage; ?>);">
		<!-- lines-container start -->
		<div class="lines-container pos-rel padding-top-bottom-120 border-box-bottom">
			<!-- title start -->
			<h2 class="headline-xxxl text-center border-box-l-r hidden-box js-scrollanim">
				<?php the_field('recent_work_title'); ?>
			</h2><!-- title end -->

			<!-- container start -->
			<div class="container">
				<!-- filter-buttons start -->
				<div class="list list_row list_center list_margin-30px padding-top-90 js-filter-button-box js-scrollanim">
					<?php if( have_rows('recent_work_category') ): ?>
					<?php 
						$i = 1;
						$dotclass = ".";
						while( have_rows('recent_work_category') ): the_row(); ?>
							<?php if($i=1) { ?>
							<button class="list__item js-filter-button anim-fade js-pointer-small js-filter-button-active" data-filter="<?php the_sub_field('filter_data_name'); ?>">
								<span class="fill-btn" data-text="<?php the_sub_field('buttone_title'); ?>"><?php the_sub_field('buttone_title'); ?></span>
							</button>
							<?php } else { ?>
							<button class="list__item js-filter-button anim-fade js-pointer-small js-filter-button-active" data-filter="<?php the_sub_field('filter_data_name'); ?>">
								<span class="fill-btn" data-text="<?php the_sub_field('buttone_title'); ?>"><?php the_sub_field('buttone_title'); ?></span>
							</button>
							<?php } ?>
					<?php
						endwhile;
						$i++;
						endif; 
					?>
				</div><!-- filter-buttons end -->

				<!-- js-isotope-filter-grid-box start -->
				<div class="js-isotope-filter-grid-box">
					<?php if( have_rows('recent_work_tab') ): ?>
					<?php 	while( have_rows('recent_work_tab') ): the_row(); ?>
								<div class="padding-top-60 grid-item-50-50-100 js-isotope-filter-grid-item <?php the_sub_field('class_name'); ?>">
									<a href="<?php the_sub_field('recent_work_url'); ?>" class="grid-margin-box hover-box pos-rel js-touch-hover-scroll js-animsition-link js-pointer-large">
										<div class="border-box anim-overlay js-scrollanim">
											<?php $workimage = get_sub_field('recent_work_image');?>
											<img src="<?php echo $workimage; ?>" alt="project">
										</div>
										<!-- content start -->
										<div class="pos-abs pos-bottom-center text-center width-100perc border-box-l-r">
											<h3 class="hidden-box d-inline-block">
												<span class="headline-xxxs text-bg-red padding-left-right-30 hover-reveal"><?php the_sub_field('recent_work_headline'); ?></span>
											</h3><br>
											<span class="hidden-box d-inline-block">
												<span class="subhead-xxs text-bg-black padding-left-right-30 hover-reveal tr-delay-02"><?php the_sub_field('recent_work_subheading'); ?></span>
											</span>
										</div><!-- content end -->
									</a>
								</div><!-- grid-item end -->
					<?php
							endwhile;
						endif; ?>
				</div><!-- js-isotope-filter-grid-box end -->
			</div><!-- container end -->
		</div><!-- lines-container end -->
	</section><!-- portfolio end -->
	
	<!-- partners start -->
	<?php $partnerbackgroundimage = get_field('partner_background_image'); ?>
	<section class="lines-section pos-rel section-bg-dark-1" style="background-image: url(<?php echo $partnerbackgroundimage; ?>);background-size: cover;">
		<!-- lines-container start -->
		<div class="lines-container pos-rel flex-min-height-100vh border-box-bottom">
			<!-- flex-container start -->
			<div class="padding-bottom-120 padding-top-30 width-100perc flex-container">
				<!-- column start -->
				<div class="four-columns column-100-100 padding-top-90">
					<div class="line-col-l-r-margin-20 js-scrollanim">
						<!-- title start -->
						<h2 class="headline-s">
							<?php the_field('heading_title'); ?>
						</h2><!-- title end -->
						<p class="body-text-s margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('partner_discription'); ?></p>
					</div>
				</div><!-- column end -->

				<!-- column start -->
				<div class="eight-columns column-100-100 flex-container padding-top-90">
					<!-- logo box start -->
					
					<?php if( have_rows('our_clients') ):
						$j = 1;
						while( have_rows('our_clients') ) : the_row();  ?> 
							
							<?php if( $j == 8 ) { ?>
							<!-- empty spot start -->
							<div class="three-columns client-logo-border">
									<div class="client-logo-border__inner hover-box pos-rel">
								<a href="#" class="three-columns client-logo-border js-pointer-large">
									<div class="client-logo-border__inner hover-box pos-rel js-scrollanim">
										<div class="padding-left-right-20 subhead-xxs text-center pos-abs pos-center-center">
											<span class="anim-text-double-fill" data-text="This spot">This spot</span><br>
											<span class="anim-text-double-fill tr-delay-02" data-text="awaits">awaits</span><br>
											<span class="anim-text-double-fill tr-delay-04" data-text="You">You</span>
										</div>
										<div class="border-left-anim in-10px center red"></div>
										<div class="border-top-anim in-10px center red"></div>
										<div class="border-right-anim in-10px center red"></div>
										<div class="border-bottom-anim in-10px center red"></div>
									</div>
								</a><!-- empty spot end -->
							<?php } else { ?>
								<?php $ClinetLogo = get_sub_field('clients'); ?>
								<?php $ClinetImage = get_sub_field('clients-logo'); ?>
								<div class="three-columns client-logo-border">
									<div class="client-logo-border__inner hover-box pos-rel">
										<img class="client-logo client-hover-out pos-abs pos-center-center" src="<?php echo $ClinetLogo; ?>" alt="clients logo">
										<img class="client-logo client-hover-in pos-abs pos-center-center" src="<?php echo $ClinetImage; ?>" alt="clients logo">
									</div>
								</div>
							<?php } ?>
					<?php
						$j++;
						endwhile;
						endif;
					?>
					
					
					
				</div><!-- column end -->
			</div><!-- flex-container end -->
		</div><!-- lines-container end -->
	</section><!-- partners end -->
	
	<!-- team start -->
	<?php 
		$TeamMemberbackgroundimage = get_field('team_backgroun_image_digicrew'); 
		$partnerbackgroundimage = get_field('partner_background_image'); 
	?>
	<section class="lines-section pos-rel section-bg-dark-1" style="background-image: url(<?php echo $partnerbackgroundimage; ?>);background-size: cover;">
		<!-- lines-container start -->
		<div class="lines-container pos-rel padding-top-bottom-120 border-box-bottom">
			<!-- title start -->
			<h2 class="headline-xxl text-center border-box-l-r hidden-box js-scrollanim">
				<span class="anim-slide">
					 <span class="text-color-red"><?php the_field('digicrerw_heading'); ?>‌</span> 
				</span>
			</h2><!-- title end -->

			<!-- flex-container start -->
			<div class="container flex-container flex-justify-center padding-top-30">
				
				<?php
					if( have_rows('team') ):
						while( have_rows('team') ) : the_row(); ?>
							<!-- column start -->
							<div class="four-columns padding-top-60">
								<div class="column-l-r-margin-10 hover-box pos-rel js-touch-hover-scroll">
									<div class="border-box content-bg-dark-1 anim-overlay js-scrollanim">
										<?php $team_image = get_sub_field('team_member_image'); ?>
										<img class="img-hover-opacity" src="<?php echo $team_image; ?>" alt="<?php the_sub_field('teame_member_name'); ?>">
									</div>
									<!-- content start -->
									<div class="pos-abs pos-bottom-center text-center width-100perc border-box-l-r">
										<h4 class="hidden-box">
											<span class="headline-xxxs hover-slide"><?php the_sub_field('teame_member_name'); ?></span>
										</h4>
										<p class="hidden-box margin-top-5">
											<span class="subhead-xxs hover-slide tr-delay-02"><?php the_sub_field('designation'); ?></span>
										</p>
									</div><!-- content end -->
								</div>
							</div><!-- column end -->
				<?php	endwhile;
					endif;
				?>
				
				
				

			
			</div><!-- flex-container end -->
		</div><!-- lines-container end -->
	</section><!-- team end -->
	
	
<?php get_footer(); ?>