<?php
/**
* Template Name: Contact Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<?php $backimage = get_field('back_ground_image'); ?>
<section id="up" class="pos-rel js-parallax-bg" style="background-image:url(<?php echo $backimage; ?>)">
			<!-- bg-overlay -->
				<div class="bg-overlay-black"></div>
				<!-- pos-rel start -->
				<div class="pos-rel flex-min-height-100vh">
					<div class="padding-top-bottom-120 container small after-preloader-anim">
						<!-- title start -->
						<?php the_field('back_ground_content'); ?>	
						<!-- title end -->
						
					</div>
				</div><!-- pos-rel end -->
			</section><!-- pos-rel end -->

			<!-- contact us start -->
			<section id="down" class="pos-rel section-bg-dark-1">
				<!-- pos-rel start -->
				<div class="pos-rel flex-min-height-100vh">
					<!-- container start -->
					<div class="padding-top-bottom-120 container">
						<!-- content start -->
						<div class="js-scrollanim">
							<h2 class="headline-l">
								<span data-text="Contact Us"><?php the_field('page_title'); ?></span>
								<!-- <span class="anim-text-double-fill" data-text="Contact Us">Contact Us</span> -->
							</h2>
							
						</div><!-- content end -->

						<!-- flex-container start -->
						<div class="flex-container padding-top-30">
							<!-- column start -->
							<div class="four-columns column-50-100 padding-top-60">
								<div class="column-r-margin-20 js-scrollanim">
									<!-- <p class="headline-xxxxs anim-text-double-fill invert" data-text="Email us">Email us</p> -->
									<p class="headline-xxxxs invert" data-text="Email us">Email us</p>
									<div class="padding-top-20">
										
										<h6 class="anim-fade tr-delay-02">
											<a href="mailto:<?php the_field('email'); ?>" class="subhead-xxs text-color-dadada text-hover-to-red js-pointer-small"><i class="far fa-envelope icon"></i> <?php the_field('email'); ?></a>
										</h6><br>
										
									</div>
								</div>
							</div><!-- column end -->

							<!-- column start -->
							<div class="four-columns column-50-100 padding-top-60">
								<div class="column-r-margin-20 js-scrollanim">
									 
								</div>
							</div><!-- column end -->

							<!-- column start -->
							<div class="four-columns column-50-100 padding-top-60">
								<div class="column-r-margin-20 js-scrollanim">
									<!-- <p class="headline-xxxxs anim-text-double-fill invert" data-text="Call us">Call us</p> -->
									<p class="headline-xxxxs invert" data-text="Call us">Call us</p>
									<div class="padding-top-20">
										<h6 class="anim-fade tr-delay-01">
											<a href="https://api.whatsapp.com/send?phone='..<?php the_field('contact_number'); ?>'" class="subhead-xxs text-color-dadada text-hover-to-red js-pointer-small"><i class="fab fa-whatsapp icon"></i><?php the_field('contact_number'); ?></a>
										</h6>
									</div>
								</div>
							</div><!-- column end -->
						</div><!-- flex-container end -->

						<!-- social btns start -->
						<div class="padding-top-90">
							<ul class="list list_row list_margin-30px js-scrollanim">
								<?php
									// Check rows exists.
									if( have_rows('socila_link') ):
									    // Loop through rows.
									    while( have_rows('socila_link') ) : the_row();
									        // Load sub field value.
									        $social_link_name = get_sub_field('social_link_name');
									        $social_link_attacthment = get_sub_field('social_link_attacthment');
									        ?>
									        <li class="list__item anim-fade">
												<a href="https://www.facebook.com/digidrunk" class="fill-btn text-color-979797 js-pointer-small" data-text="<?php echo $social_link_name; ?>"><?php echo $social_link_name; ?></a>
											</li>
									     <?php   
									    // End loop.
									    endwhile;
									endif;
								?>	
								
							</ul>
						</div><!-- social btns end -->
					</div><!-- container end -->
				</div><!-- pos-rel end -->
			</section><!-- contact us end -->

			<!-- form section start -->
			<section class="pos-rel bg-img-cover" style="background-image:url(http://www.digidrunk.in/wp-content/uploads/2020/10/download-1.jpg)">
				<!-- bg-overlay -->
				<div class="bg-overlay-black"></div>
				<!-- pos-rel start -->
				<div class="pos-rel flex-min-height-100vh">
					<!-- container start -->
					<div class="container small padding-top-bottom-120 form-box">
						<h4 class="headline-xxxxs text-center">Lets Get In Touch!</h4>
						<!-- flex-container start -->
						<?php echo do_shortcode('[contact-form-7 id="112" title="Contact New"]'); ?>
					</div><!-- container end -->

					<!-- alert start -->
					<div class="js-popup-fade" id="m_sent">
						<div class="js-popup">
							<div class="popup-icon">
								<i class="fas fa-check"></i>
							</div>
							<p class="popup-alert headline-xs">
								Thank you!<br>
								Your submission<br>
								has been received!
							</p>
							<div class="js-popup-close js-pointer-large">
								<div class="flip-btn" data-text="Close">Close</div>
							</div>
						</div>
					</div><!-- alert end -->

					<!-- alert start -->
					<div class="js-popup-fade" id="m_err">
						<div class="js-popup">
							<div class="popup-icon">
								<i class="fas fa-times"></i>
							</div>
							<p class="popup-alert headline-xs">Error</p>
							<div class="js-popup-close js-pointer-large">
								<div class="flip-btn" data-text="Close">Close</div>
							</div>
						</div>
					</div><!-- alert end -->
				</div><!-- pos-rel end -->
			</section><!-- form section end -->
<?php get_footer(); ?>