<?php
/**
* Template Name: Search Marketing Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
<?php $backimage = get_field('search_backgroung_image'); ?>
<section id="up" class="pos-rel" style="background-image: url(<?php echo $backimage; ?>);background-size: cover;">
    <!-- pos-rel start -->
    <div class="pos-rel flex-min-height-100vh">
        <div class="container padding-top-bottom-120 after-preloader-anim">
            <h3 class="headline-xxxs hidden-box">
                <span class="anim-slide"><?php the_field('page_title');?></span>
            </h3>
            <h2 class="subhead-xxl margin-top-20 anim-text-reveal tr-delay-03"><?php the_field('search_marketing_content');?></h2>
                <?php if( have_rows('search_items') ): ?>
            <ul class="list subhead-xxs text-color-b0b0b0 margin-top-30 js-scrollanim js-scrollanim-active">
                   
                    <?php while( have_rows('search_items') ): the_row(); ?>
                        <li class="list__item red dot hidden-box">
                            <p class="anim-slide"><?php the_sub_field('search_item_name'); ?></p>
                        </li>
                    <?php endwhile; ?>
            </ul>
                <?php endif; ?>
        </div>
    </div><!-- pos-rel end -->
</section><!-- page head end -->

<section id="down" class="pos-rel section-bg-light-1" data-midnight="black">
    <h3><!-- pos-rel start --></h3>
    <div class="pos-rel flex-min-height-100vh">
        <div class="container ">
            <div class="flex-container">
                <?php if( have_rows('search_service') ): ?>
                    <?php while( have_rows('search_service') ): the_row(); ?>
                        <div class="six-columns column-100-100 ">
                            <div class="column-r-margin-40-999 js-scrollanim">
                                            <span class="subhead-xxl text-color-red d-block hidden-box">
                                                <span class="anim-slide">0<?php the_sub_field('search_counter_number'); ?></span>
                                            </span>
                                <h3 class="headline-xxxs text-color-black hidden-box">
                                    <span class="anim-slide tr-delay-01"><?php the_sub_field('search_name'); ?></span>
                                </h3>
                                <p class="body-text-s text-color-black margin-top-20 anim-text-reveal tr-delay-02"><?php the_sub_field('search_content'); ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>		
            </div>
        </div>
        <!-- container end -->
    </div>
</section><!-- work process end -->
<?php get_footer(); ?>