<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'digidrunk' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'fo)~%5jX5/?I)6d:X3Dtx@i;H@9)~MsD~LUlVLW1tV^Wc{Mk^}gB1{U[4n.o*hv4' );
define( 'SECURE_AUTH_KEY',  '):9-TV[t@qi8LouC]qY&SfHjubpioz>A+~K41p}_&/te]Zn_5zUQ&3 r*pG5mfA}' );
define( 'LOGGED_IN_KEY',    'cNJmZvz,pTmde0jF+!+`KIe8Aa*nV$km1s*P0+(vO$0bJVl2C]],Z}!_Gbl_:mcy' );
define( 'NONCE_KEY',        '!ZqV`SDKRs(D%,It0Btx$ 9#NG~ =coJc_To8GEXx63O_ 5^3=RL{K&Cj)JBhi 7' );
define( 'AUTH_SALT',        'Hhr|tS/97$8]I!~RGp4|{0:vMWuGGW)Y>WV&>5@IQ;&7#1t[!j6K{TH!fXS%DyL!' );
define( 'SECURE_AUTH_SALT', '$QPmJ1eEq:9y16O|S7cU#=+la{{C] XY[J7bx]?=f&$NpcV&Wl:SU,sY>?jxG^Hl' );
define( 'LOGGED_IN_SALT',   'oxd9 :^Muwv1/r#|T{BF_Ttbq;4Tc*njzZPRPpU1.zl{lV9[cR(b[^0oTOABEEJ(' );
define( 'NONCE_SALT',       'Dl=wBrr8G}*u^^b<0]UUFf |+P/R7Y@lm@9G_L}Z!sQ%XpQy&bd^8ZebRE?Cl)U@' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', FALSE );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
